﻿using UnityEngine;
using System.Collections;
using UnityEditor;
namespace GGJ18
{
    [CustomEditor(typeof(MusicManager))]
    public class MusicManagerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            MusicManager myScript = (MusicManager)target;
            if (GUILayout.Button("ChangeMusic"))
            {
                myScript.ChangeSong();
            }
        }
    }
}