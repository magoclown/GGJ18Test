﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace GGJ18
{
    [CustomEditor(typeof(ChangeValueBar))]
    public class ChangeValueBarEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            ChangeValueBar myScript = (ChangeValueBar)target;
            if (GUILayout.Button("Change Value"))
            {
                myScript.TestChange();
            }
        }
    }
}