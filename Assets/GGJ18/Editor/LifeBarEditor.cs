﻿using UnityEngine;
using System.Collections;
using UnityEditor;
namespace GGJ18
{
    [CustomEditor(typeof(LifeBar))]
    public class LifeBarEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            LifeBar myScript = (LifeBar)target;
            if (GUILayout.Button("Change Value"))
            {
                myScript.TestChange();
            }
        }
    }
}