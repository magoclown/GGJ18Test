﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ18
{
    public class StatusManager : MonoBehaviour
    {
        public List<statusEffect> statusColliders = new List<statusEffect>();

        // Use this for initialization
        void Start()
        {
            rerollColliders();
        }

        public void rerollColliders() {
            foreach (statusEffect statuscollider in statusColliders)
            {
                statuscollider.GetComponent<statusEffect>().ChangeStatus((statusEffect.Status)Random.Range(0, 3));
            }
        }
    }
}
