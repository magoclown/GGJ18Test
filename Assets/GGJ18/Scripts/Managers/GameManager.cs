﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace GGJ18
{
    public class GameManager : MonoSingleton<GameManager>
    {
        #region Properties
        private bool pressedKey = true, gameEnded = false;
        private GameObject canvas, pausePanel, endPanel, hackingHud;
        private TimeManager timeM;
        [SerializeField]
        private RoundManager roundManager;
        [SerializeField]		
	    private StatusManager statusManager;

        [SerializeField]
        private AudioClip deathSong;
        #endregion
        #region Unity
        #endregion
        #region Methods
        private void Awake()
        {
            canvas = GameObject.FindGameObjectWithTag("CanvasTag");
            pausePanel = GameObject.FindGameObjectWithTag("PausePanelTag");
            endPanel = GameObject.FindGameObjectWithTag("GameOverTag");
            hackingHud = GameObject.FindGameObjectWithTag("HackingHud");
            if (pausePanel != null)
                pausePanel.SetActive(false);
            if (endPanel != null)
                endPanel.SetActive(false);
            if (hackingHud != null)
                hackingHud.SetActive(false);
            if (GameObject.FindGameObjectWithTag("TimeManagerTag") != null)
                timeM = GameObject.FindGameObjectWithTag("TimeManagerTag").GetComponent<TimeManager>();
        }
        private void Update()
        {
            if (gameEnded)
                return;
            if (Input.GetKeyDown("escape"))
            {
                if (pressedKey)
                {
                   //Debug.Log("ESC PRESSED FOR PAUSE BUTTON");
                    if(pausePanel != null)
                     pausePanel.SetActive(true);
                    timeM.Pause(true);
                }
                else
                {
                   //Debug.Log("ESC PRESSED FOR RELEASE PAUSE BUTTON");
                    if (pausePanel != null)
                     pausePanel.SetActive(false);
                    timeM.UnPause(true);
                }
                pressedKey = !pressedKey;
            }
            else if(Input.GetKeyDown("space"))
            {
                endGame();
            }
        }
        public void unPausedGame()
        {
            pausePanel.SetActive(false);
            timeM.UnPause(true);
            pressedKey = !pressedKey;
        }
        private void startGame() { }
        public void goToMainMenu()
        {
            SceneManager.LoadScene("TitleScreen");
        }
        public void restartGame()
        {
            unPausedGame();
            string currentSceneName = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(currentSceneName);
        }
        public void endGame()
        {
            MusicManager.instance.ChangeSong(deathSong);
            gameEnded = true;
            //timeM.Pause(true);
            pausePanel.SetActive(false);
            endPanel.SetActive(true);
            roundManager.GameOver();
        }
        public void characterIsHacked(int lifeSpan)
        {
            Difficulties diff = (Difficulties)lifeSpan;
            hackingHud.SetActive(true);
            hackingHud.GetComponent<Hackimg>().CallDifficulty(diff);
        }
        public void characterHackedEnd(bool state)
        {
            hackingHud.SetActive(false);
            if (state)
            {
                GameObject.FindGameObjectWithTag("Player").GetComponent<Character>().setHackResult(false);
                return;
            }
            GameObject.FindGameObjectWithTag("Player").GetComponent<Character>().setHackResult(true);
        }
        public void RemoveEnemy()
        {
            if (roundManager != null)
            {
                roundManager.decreaseEnemies();
            }
        }

        public void endWave()
        {
            GameObject.FindGameObjectWithTag("StatusManager").GetComponent<StatusManager>().rerollColliders();
        }
        #endregion


    }
}