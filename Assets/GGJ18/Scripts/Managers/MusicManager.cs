﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ18
{
    public enum TypeAudio
    {
        Title,
        Game,
        Credits,
        Death,
        Pause
    }
    public class MusicManager : MonoSingleton<MusicManager>
    {
        [SerializeField]
        private AudioClip actualClip;
        [SerializeField]
        private AudioClip newClip;

        [SerializeField]
        private AudioSource audioSource;
        [SerializeField]
        private AudioSource newSource;
        
        private AudioSource mediumSource;
        [SerializeField]
        private float timeFade;
        [SerializeField]
        private TypeAudio typeAudio;

        [SerializeField]
        private AudioClip testClip;
        [SerializeField]
        private TypeAudio testAudio;

        #region Unity
        void Start()
        {
            if (actualClip != null)
            {
                audioSource.clip = actualClip;
                audioSource.Play();
            }
        }
        #endregion
        #region Methods
        public void ChangeSong(AudioClip newSong, TypeAudio newType = TypeAudio.Game)
        {
            newClip = newSong;
            EnterFadeOut();
        }
        public void ChangeSong(AudioClip newSong)
        {
            newClip = newSong;
            EnterFadeOut();
        }

        private void EnterFadeOut()
        {
            StopAllCoroutines();
            StartCoroutine(FadeOut());
            StartCoroutine(FadeIn());
        }

        private void ChangeSource()
        {
            mediumSource = audioSource;
            audioSource = newSource;
            newSource = mediumSource;
            actualClip = newClip;
            newClip = null;
        }
        #endregion
        #region Coroutines
        IEnumerator FadeOut()
        {
            float startVolume = audioSource.volume;
            float fadeTime = timeFade;
            while (audioSource.volume > 0)
            {
                audioSource.volume -= startVolume * Time.deltaTime / fadeTime;

                yield return null;
            }
            audioSource.volume = 0;
        }

        IEnumerator FadeIn()
        {
            newSource.clip = newClip;
            newSource.Play();
            newSource.volume = 0.0f;
            float fadeTime = timeFade;
            while (newSource.volume < 1)
            {
                newSource.volume += Time.deltaTime / fadeTime;
                yield return false;
            }
            newSource.volume = 1;
            ChangeSource();
            yield return null;
        }
        #endregion
        #region Test
        public void ChangeSong()
        {
            ChangeSong(testClip, testAudio);
        }
        #endregion
    }
}