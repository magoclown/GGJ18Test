﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ18
{
    public class TimeManager : MonoSingleton<TimeManager>
    {
        #region Events
        public event System.EventHandler<System.EventArgs> GamePaused;

        public event System.EventHandler<System.EventArgs> GameUnPaused;
        #endregion
        #region Properties
        protected static bool paused;
        public bool isPaused { get { return paused; } }
        #endregion
        #region Methods
        virtual public void OnGamePaused()
        {
            if (GamePaused != null)
            {
                GamePaused(this, null);
            }
        }

        virtual public void OnGameUnPaused()
        {
            if (GameUnPaused != null)
            {
                GameUnPaused(this, null);
            }
        }

        public void Pause(bool setTimeScale)
        {

            paused = true;
            if (setTimeScale) Time.timeScale = 0;

            Debug.Log("Pause paused : " + paused.ToString() + "setTimeScale : " + setTimeScale.ToString());
            OnGamePaused();
        }

        public void UnPause(bool setTimeScale)
        {
                paused = false;
                if (setTimeScale) Time.timeScale = 1;
                OnGameUnPaused();
        }
        #endregion
    }
}