﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ18
{
    public class statusEffect : MonoBehaviour
    {

        public enum Status { normal, nullify, amplify, interference };
        public Status status;
        public float multiplier;

        public GameObject particleNull;
        public GameObject particleAmplify;
        public GameObject particleHide;

        // Use this for initialization
        void Start()
        {

        }

        public void ChangeStatus(Status newStatus)
        {
            status = newStatus;
            particleNull.SetActive(false);
            particleAmplify.SetActive(false);
            particleHide.SetActive(false);
            switch (newStatus)
            {
                case Status.nullify:
                    particleNull.SetActive(true);
                    break;
                case Status.amplify:
                    particleAmplify.SetActive(true);
                    break;
                case Status.interference:
                    particleHide.SetActive(true);
                    break;
                case Status.normal:
                    break;
            }

        }

        void OnTriggerExit2D(Collider2D coll)
        {
            if (coll.gameObject.tag == "Player" && status == Status.nullify)
            {
                Debug.Log("Nullifying Hack Over.");
            }

            if (coll.gameObject.tag == "Enemy" && status == Status.amplify)
            {
                coll.gameObject.GetComponent<Enemy>().ResetMultiplier();
                Debug.Log("Amplifying Hacking for Enemy Over.");
            }

            if (status == Status.interference)
            {
                if (coll.gameObject.GetComponent<SpriteRenderer>() != null)
                {
                    coll.gameObject.GetComponent<SpriteRenderer>().enabled = true;
                    Debug.Log("Interference Over.");
                }
            }
        }

        void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.gameObject.tag == "Player" && status == Status.nullify)
            {
                coll.gameObject.GetComponent<Character>().hackSetDecrement(multiplier); 
                Debug.Log("Nullifying Hack.");
            }

            if (coll.gameObject.tag == "Enemy" && status == Status.amplify)
            {
                coll.gameObject.GetComponent<Enemy>().SetMultiplier(multiplier);
                Debug.Log("Amplifying Hacking for Enemy.");
            }

            if (status == Status.interference)
            {
                if (coll.gameObject.GetComponent<SpriteRenderer>() != null)
                {
                    coll.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                    Debug.Log("Interference.");
                }
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
