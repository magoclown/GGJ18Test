﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace GGJ18
{
    public enum Difficulties
    {
        Easy = 3,
        Medium = 2,
        Hard = 1
    };

    public class Hackimg : MonoBehaviour
    {
        //public Image canvas;
        Button greenButton;
        List<Button> allButtons = new List<Button>(),
                   activeButtons = new List<Button>(),
                   usedButtons = new List<Button>(),
                   errorButtons = new List<Button>();
        public float timePerButton = 4.0f;
        public Slider slider;

        public void CallDifficulty(Difficulties dificulty)
        {
            switch (dificulty)
            {
                case Difficulties.Easy:
                    this.PrepareHack(3);
                    break;
                case Difficulties.Medium:
                    this.PrepareHack(5);
                    break;
                case Difficulties.Hard:
                    this.PrepareHack(7);
                    break;
            }
        }

        public void PrepareHack(int buttonsQty)
        {
            foreach (Button btn in allButtons)
            {
                btn.image.color = Color.white;
                btn.onClick.RemoveAllListeners();
            }
            allButtons.Clear();
            activeButtons.Clear();
            usedButtons.Clear();
            errorButtons.Clear();

            allButtons.AddRange(GetComponentsInChildren<Button>(true));
            Debug.Log("VALOR DE BOTONES: " + (allButtons.Count - activeButtons.Count));
            for (int i = 0; i < buttonsQty; i++)
            {
                activeButtons.Add(allButtons.Except(activeButtons).ToList()[Random.Range(0, (allButtons.Count - activeButtons.Count))]);
            }

            greenButton = activeButtons[Random.Range(0, activeButtons.Count)];
            greenButton.image.color = Color.green;
            greenButton.onClick.AddListener(onClickCorrectButton);
            
            errorButtons.AddRange(allButtons.Where(e => e != greenButton));
            foreach (Button btn in errorButtons)
            {
                btn.onClick.AddListener(onClickIncorrectButton);
            }
            StartCoroutine(this.StartButtonTimer(timePerButton));
        }

        /*void Start()
        {
            CallDifficulty(Difficulties.Medium);
        }*/

        IEnumerator StartButtonTimer(float time)
        {
            Debug.Log("Time: " + time);
            Debug.Log("slider.value: " + slider.value);
            slider.maxValue = time;
            slider.value = time;
            if (slider != null)
            {
                float alpha = 0.0f;
                while (alpha < time)
                {
                    Debug.Log("alpha: " + alpha);
                    slider.value -= 0.1f;
                    alpha += 0.1f;

                    if (slider.value <= 0)
                        break;

                    yield return new WaitForSeconds(0.1f);
                }

                if (alpha >= time)
                {
                    this.onTimeLeftEnd();
                }
            }
        }

        public void generateNextGreenButton()
        {
            greenButton.image.color = Color.white;
            greenButton.onClick.RemoveListener(onClickCorrectButton);

            foreach (Button btn in errorButtons)
            {
                btn.onClick.RemoveListener(onClickIncorrectButton);
            }

            if (activeButtons.Count - usedButtons.Count > 0)
            {
                greenButton = (activeButtons.Except(usedButtons)).ToList()[Random.Range(0, (activeButtons.Count - usedButtons.Count))];
                greenButton.image.color = Color.green;
                greenButton.onClick.AddListener(onClickCorrectButton);

                errorButtons.Clear();
                errorButtons.AddRange(allButtons.Where(e => e != greenButton));
                foreach (Button btn in errorButtons)
                {
                    btn.onClick.AddListener(onClickIncorrectButton);
                }
            }
        }

        public void onClickCorrectButton()
        {
            if ((activeButtons.Count - usedButtons.Count) > 1)
            {
                usedButtons.Add(greenButton);
                this.generateNextGreenButton();
            }
            else
            {
                StopAllCoroutines();
                GameManager.instance.characterHackedEnd(true);
            }
            
        }

        public void onClickIncorrectButton()
        {
            StopAllCoroutines();
            GameManager.instance.characterHackedEnd(false);
        }

        public void onTimeLeftEnd()
        {
            StopAllCoroutines();
            GameManager.instance.characterHackedEnd(false);
        }
    }
}