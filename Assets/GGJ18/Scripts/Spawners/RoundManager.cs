﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GGJ18
{
    public class RoundManager : Pausable
    {

        public List<GameObject> waves;
        public List<AudioClip> audioClips;
        public GameObject musicManager;
        public int enemiesPerRound;
        
        int wavesLeft;
        int waveIndex;
        int enemiesLeft;
        bool roundOver;

        override protected void Start()
        {
            base.Start();
            waveIndex = 0;
            roundOver = false;
            wavesLeft = waves.Count;
        }

        void Update()
        {
            
            if (!roundOver)
            {
                
                if (enemiesLeft == 0)
                {
                    if (wavesLeft > 0)
                    {
                        GameManager.instance.endWave();
                        startWave();
                    }
                    else
                    {
                        roundOver = true;
                    }
                }
            }
        }

        void startWave()
        {
            if (waves[waveIndex] != null)
            {
                waves[waveIndex].GetComponent<Wave>().spawnWave();

                enemiesLeft = waves[waveIndex].GetComponent<Wave>().numberOfEnemies;
                musicManager.GetComponent<MusicManager>().ChangeSong(audioClips[waveIndex]);

                waveIndex++;
                wavesLeft--;
            }
        }

        public void decreaseEnemies()
        {
            enemiesLeft-- ;
        }

        public bool getIsRoundOver()
        {
            return roundOver;
        }

        public void GameOver()
        {
            foreach (GameObject gO in waves)
            {
                if (gO != null)
                {
                    gO.GetComponent<Wave>().GameOver();
                    Destroy(gO);
                }
            }
            Destroy(this.gameObject);
        }
    }
}