﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GGJ18
{
    public class Wave : Pausable
    {
        public List<GameObject> enemyPrefabs;
        public int numberOfEnemies;
        [Range(1, 20)]
        public int enemyMinLife;
        [Range(1, 20)]
        public int enemyMaxLife;
        [Range(1, 5)]
        public float startWait;

        GameObject[] spawnPoints;
        GameObject currentPoint;
        GameObject enemy;

        public List<GameObject> actualEnemies;

        int spawnIndex;
        int prefabIndex;
        int enemyLifeSpan;

        // Use this for initialization
        override protected void Start()
        {
            base.Start();
        }

        public void spawnWave() {
            StartCoroutine(SpawnEnemies());
        }

        IEnumerator SpawnEnemies()
        {

            yield return new WaitForSeconds(startWait);

            for (int i = 0; i < numberOfEnemies; i++)
            {
                spawnPoints = GameObject.FindGameObjectsWithTag("point");
                spawnIndex = Random.Range(0, spawnPoints.Length);
                currentPoint = spawnPoints[spawnIndex];

                prefabIndex = Random.Range(0, enemyPrefabs.Count);
                enemy = enemyPrefabs[prefabIndex];

                Vector3 spawnPosition = new Vector3(currentPoint.transform.position.x, currentPoint.transform.position.y, currentPoint.transform.position.z);
                Quaternion spawnRotation = Quaternion.identity;
                GameObject gO = Instantiate(enemy, spawnPosition, spawnRotation) as GameObject;
                enemyLifeSpan = Random.Range(enemyMinLife, enemyMaxLife);
                gO.GetComponent<Enemy>().Init(enemyLifeSpan);
                actualEnemies.Add(gO);
            }
            
        }

        public void GameOver()
        {
            foreach (GameObject gO in actualEnemies)
            {
                if(gO != null)
                    Destroy(gO);
            }
        }
    }
}
