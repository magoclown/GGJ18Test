﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GGJ18
{
    [RequireComponent(typeof(Rigidbody2D))]

    public class Character : Pausable {


        public bool hasControl;

        private float hackMeter;
        private float hackMaxMeter = 100;
        private bool hackStatus;

        private int lifeMeter = 3;
        private int lifeMaxMeter = 3;

        private Vector2 _lastDirection;

        private bool _isMoving;

        [SerializeField]
        private Rigidbody2D _RGB;


        [SerializeField]
        private Animator _Anim;


        [SerializeField]
        private BoxCollider2D _boxCollider;

        private Vector2 _deltaForce;
        public float _speed = 2;
        public List<Enemy> enemies;

        [SerializeField]
        private ChangeValueBar hackBar;
        [SerializeField]
        private LifeBar lifeBar;

        private void Awake()
        {
            
            _RGB = GetComponent<Rigidbody2D>();
            _Anim = GetComponent<Animator>();
            _boxCollider = GetComponent<BoxCollider2D>();

        }
        override protected void Start ()
        {
            base.Start();
            hasControl = true;

            hackMeter = 0;
            hackStatus = false;

            lifeMeter = lifeMaxMeter;

            _RGB.gravityScale = 0;
            _RGB.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
	
	    void Update ()
        {
            if (isPause)
                return;

            if (hasControl)
                checkInput();

            if (!hackStatus)
                hackStatusCheck();
        }

        public void addEnemy(Enemy obj)
        {
            enemies.Add(obj);
            StopCoroutine("decreaseHackCoroutine");
        }


        public void removeEnemy(Enemy obj)
        {
            enemies.Remove(obj);

            if (enemies.Count <= 0)
                StartCoroutine("decreaseHackCoroutine",1);
        }

        IEnumerator decreaseHackCoroutine(float sec)
        {
            while (true)
            {
                hackAutoDecrease();

                yield return new WaitForSeconds(sec);

            }
        }

        void hackStatusCheck()
        {
            hackBar.ChangeValue(hackMeter);
            if (hackMeter >= hackMaxMeter && !hackStatus)
            {
                hack();
            }
        }

        void hackAutoDecrease()
        {
            if (hackMeter <= 0)
            {
                hackMeter = 0;
                return;
            }
                
            hackMeter -= 3;
            Debug.Log("Decrementando");
        }

        void hack()
        {
            Debug.Log("Enter Hack State");
            _Anim.SetInteger("Status", 10);
            hackStatus = true;
            GameManager.instance.characterIsHacked(lifeMeter);
        }
       
        void dead()
        {
            Debug.Log("vidas antes:" + lifeMeter);
            lifeMeter -= 1;
            hackMeter = 0;
            lifeBar.ChangeValue(lifeMeter);
            Debug.Log("vidas despues:" + lifeMeter);
            if (lifeMeter < 1)
                gameOver();
        }

        void hackSuccess()
        {
            //codigo cuando logras el hack
            hackMeter = 0;
            _Anim.SetInteger("Status", 5);
        }

        void gameOver()
        {
            //cogido gameOver
            _Anim.SetInteger("Status", 30);
        }

        public void CallGameOver()
        {
            GameManager.instance.endGame();
        }

        void checkInput()
        {
            _isMoving = false;

            var _H = Input.GetAxisRaw("Horizontal");
            var _V = Input.GetAxisRaw("Vertical");

            if (_H < 0 || _H > 0 || _V < 0 || _V > 0)
                _isMoving = true;

            _deltaForce = new Vector2(_H, _V);

            calculateMovement(_deltaForce * _speed * Time.deltaTime);
        }

        void calculateMovement(Vector2 varForce)
        {
            _RGB.velocity = Vector2.zero;
            _RGB.AddForce(varForce, ForceMode2D.Impulse);

            sendAnimInfo();
        }

        public void hackSetIncrement(float val) //incrementa el medidor de hack del personaje externamente
        {
            if (hackMeter <= hackMaxMeter)
            {
                hackMeter += val;
            }

        }

        public void hackSetDecrement(float val)
        {
            if (hackMeter > 0)
            {
                hackMeter -= val;
            }
        }

        public bool getHackStatus() //devuelve el estado de hack 
        {
            return hackStatus;
        }


        public void setHackResult(bool val) // realiza lo necesario segun los resultados obtenidos del Hack
        {
            hackStatus = false;
            
            if (val)
            {
                _Anim.SetInteger("Status", 20);
                dead();
                return;
            }

            hackSuccess();
        }

        void sendAnimInfo() // envia info de la animacion
        {

           
            _Anim.SetFloat("XSpeed",_RGB.velocity.x);
            _Anim.SetFloat("YSpeed", _RGB.velocity.y);


            _Anim.SetFloat("lastX", _lastDirection.x);
            _Anim.SetFloat("lastY", _lastDirection.y);


            //Debug.Log(_lastDirection.x);

            _Anim.SetBool("isMoving", _isMoving);
        }
    }
}
