﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ18 {

    public class MenuManager : MonoBehaviour
    {
        public void retryGame() { GameManager.instance.restartGame(); }
        public void continueGame() { GameManager.instance.unPausedGame(); }
        public void mainMenuGame() { GameManager.instance.goToMainMenu(); }
    }
}
