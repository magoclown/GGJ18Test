﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ18
{
    public class Enemy_AnimationToSprite : MonoBehaviour
    {
        [SerializeField]
        private Enemy enemy;

        private void Start()
        {
            if (enemy == null)
            {
                enemy = GetComponentInParent<Enemy>();
            }
        }

        public void CallFinishDestroy()
        {
            enemy.FinishDestroy();
        }
    }

}