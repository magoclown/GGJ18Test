﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ18
{
    public class EffectZone : Pausable
    {
        #region Properties
        [SerializeField]
        private float damagePerTime = 1.0f;
        [SerializeField]
        private float timeForDamage = 1.0f;

        [SerializeField]
        private float defaultMultiplier = 1.0f;
        [SerializeField]
        private float multiplier = 1.0f;

        private Character character;

        private Enemy enemy;

        #endregion
        #region Unity
        override protected void Start()
        {
            base.Start();

            enemy = GetComponentInParent<Enemy>();

            GameObject gO = GameObject.FindGameObjectWithTag("Player");
            if (gO != null)
            {
                character = gO.GetComponent<Character>();
            }
        }
        override protected void OnDisable()
        {
            base.OnDisable();
            StopAllCoroutines();
        }
        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                //other.gameObject.GetComponent<Character>().hackSetIncrement(damagePerTime * multiplier);
                character.addEnemy(enemy);
                StartCoroutine("HackingOnTime");
            }
        }

        void OnTriggerExit2D(Collider2D other)
        {
            if (other.tag == "Player")
            {
                character.removeEnemy(enemy);
                StopCoroutine("HackingOnTime");
            }
        }
        #endregion
        #region Methods
        public void SetMultiplier(float value)
        {
            multiplier = value;
        }
        public void ResetMultiplier()
        {
            multiplier = defaultMultiplier;
        }
        #endregion
        #region Coroutines
        IEnumerator HackingOnTime()
        {
            while (true)
            {
                //Debug.Log("Haz daño");
                character.hackSetIncrement(damagePerTime * multiplier);
                //Debug.Log("Send Damage: " + damagePerTime * multiplier);
                yield return new WaitForSeconds(timeForDamage);
            }
        }
        #endregion
    }
}