﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ18
{
    public class Enemy : Pausable
    {
        #region Properties
        private GameObject character;
        [SerializeField]
        private float speed = 1.0f;

        
        private float timeToDestroy;

        [SerializeField]
        private bool canMove;
        
        [SerializeField]
        private EffectZone effectZone;

        [SerializeField]
        private Animator animator;

        [SerializeField]
        private GameObject deathParticles;

        [SerializeField]
        private ChangeValueBar lifeBar;

        [SerializeField]
        private bool isTest;
        #endregion
        #region Unity
        override protected void Start()
        {
            base.Start();
            character = GameObject.FindWithTag("Player");
            if (isTest)
            {
                Init();
            }
        }
        
        void Update()
        {
            //if (isPause)
            //{
            //    return;
            //}
            if (!canMove)
                return;
            ChaseCharacter();
        }

        override protected void OnDestroy()
        {
            base.OnDestroy();
            GameManager.instance.RemoveEnemy();
        }
        #endregion
        #region Methods
        
        public void Init(float destroyAfter = 20.0f)
        {
            canMove = true;
            StartCoroutine(DestroyAfter(destroyAfter));
            lifeBar.SetNewMax(destroyAfter);
            timeToDestroy = destroyAfter;
            lifeBar.ChangeValue(timeToDestroy);
        }
        public void SetMultiplier(float value)
        {
            effectZone.SetMultiplier(value);
        }
        public void ResetMultiplier()
        {
            effectZone.ResetMultiplier();
        }
        private void ChaseCharacter()
        {
            if (character != null)
            {
                Vector3 characterPosition = character.transform.position;
                float xMove = 0.0f;
                float yMove = 0.0f;
                if (characterPosition.x < transform.position.x)
                {
                    xMove = -1.0f;
                }
                else
                {
                    xMove = 1.0f;
                }
                if (characterPosition.y < transform.position.y)
                {
                    yMove = -1.0f;
                }
                else
                {
                    yMove = 1.0f;
                }
                animator.SetFloat("PosX", Mathf.Round(xMove));
                animator.SetFloat("PosY", Mathf.Round(yMove));
                transform.Translate(new Vector3(xMove, yMove, 0.0f) * speed * Time.deltaTime);
            }
        }
        private void CallDestroy()
        {
            animator.SetBool("IsDeath", true);
            canMove = false;
            if (deathParticles != null)
            {
                GameObject gO = Instantiate(deathParticles, transform.position, Quaternion.identity, null) as GameObject;
                gO.SetActive(true);
            }
        }
        public void FinishDestroy()
        {
            Destroy(this.gameObject);
        }
        #endregion
        #region Coroutines
        IEnumerator DestroyAfter(float time)
        {
            float alpha = 0.0f;
            while (alpha < time)
            {
                while (isPause)
                {
                    yield return new WaitForEndOfFrame();
                }
                alpha += 0.1f;
                lifeBar.ChangeValue(timeToDestroy - alpha);
                yield return new WaitForSeconds(0.1f);
            }
            CallDestroy();
            
        }
        #endregion
    }
}