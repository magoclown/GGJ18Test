﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGJ18
{
    public class Pausable : MonoBehaviour
    {
        #region Properties
        protected bool isPause;
        #endregion
        #region Unity
        protected virtual void Start()
        {
            Init();
        }

        protected virtual void OnDestroy()
        {
            DoDestroy();
        }

        protected virtual void OnEnable()
        {
            Init();
        }

        protected virtual void OnDisable()
        {
            DoDestroy();
        }
        #endregion

        #region Methods
        protected virtual void Init()
        {
            TimeManager.instance.GamePaused += HandleGamePaused;
            TimeManager.instance.GameUnPaused += HandleGameUnPaused;
        }

        protected virtual void DoDestroy()
        {
            TimeManager.instance.GamePaused -= HandleGamePaused;
            TimeManager.instance.GameUnPaused -= HandleGameUnPaused;
        }

        void OnDisabled() {
            DoDestroy();
        }


        void update() {
            if (isPause)
                return;
        }
        protected virtual void HandleGameUnPaused(object sender, System.EventArgs e)
        {
            isPause = false;
        }

        protected virtual void HandleGamePaused(object sender, System.EventArgs e)
        {
            isPause = true;
        }

        #endregion
    }
}