﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
namespace GGJ18
{
    public class OnValueChange : MonoBehaviour
    {

        public AudioMixer masterMixer;

        public void SetSfxLvl(float sfxLvl)
        {
            masterMixer.SetFloat("sfxVol", sfxLvl);
        }

        public void SetBgmLvl(float sfxLvl)
        {
            masterMixer.SetFloat("bgmVol", sfxLvl);
        }
    }
}