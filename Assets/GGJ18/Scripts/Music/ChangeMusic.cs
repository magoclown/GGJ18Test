﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GGJ18
{
    public class ChangeMusic : MonoBehaviour
    {
        public AudioClip audioClip;

        public void ChangeClip()
        {
            MusicManager.instance.ChangeSong(audioClip, TypeAudio.Title);
        }
    }
}