﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace GGJ18
{
    public class LifeBar : MonoBehaviour
    {
        [SerializeField]
        private Color okayColor = Color.green;
        [SerializeField]
        private Color damageColor = Color.red;
        
        public List<Image> lifes;

        [SerializeField]
        private int testLifes;

        public void ChangeValue(int value)
        {
            DamageLifes();
            for (int i = 0; i <value; i++)
            {
                lifes[i].color = okayColor;
            }
        }

        public void ResetLifes()
        {
            foreach (Image life in lifes)
            {
                life.color = okayColor;
            }
        }

        public void DamageLifes()
        {
            foreach (Image life in lifes)
            {
                life.color = damageColor;
            }
        }

        public void TestChange()
        {
            ChangeValue(testLifes);
        }
    }
}