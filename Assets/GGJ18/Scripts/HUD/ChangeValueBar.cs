﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GGJ18
{
    public class ChangeValueBar : MonoBehaviour
    {
        [SerializeField]
        private Slider sliderBar;

        [SerializeField]
        private float testValue;

        public void ChangeValue(float value = 0)
        {
            sliderBar.value = value;
        }

        public void TestChange()
        {
            ChangeValue(testValue);
        }

        public void SetNewMax(float value)
        {
            sliderBar.maxValue = value;
        }

        public void SetNewMin(float value)
        {
            sliderBar.minValue = value;
        }

    }
}