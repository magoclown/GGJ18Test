﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

namespace GGJ18
{
    public class HideVideoOnEnd : MonoBehaviour
    {

        private void Start()
        {
            GetComponent<VideoPlayer>().loopPointReached += EndReached;
        }

        private void EndReached(VideoPlayer vp)
        {
            this.gameObject.SetActive(false);
        }

    }
}