﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace GGJ18
{
    public class ChangeScene : MonoBehaviour
    {
        public void LoadScene(string newScene)
        {
            SceneManager.LoadScene(newScene);
        }
    }
}